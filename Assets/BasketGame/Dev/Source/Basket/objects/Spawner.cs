﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Spawner : CoreSceneObject
{
    public GameObject PrefabBall;
    private int counter = 0;

    public override void Init()
    {
        base.Init();

        enabled = true;
    }
    public override void Update()
    {
        base.Update();

        if(counter % 20 == 0)
        {
            counter = 0;
            Vector3 pos = Utils.GetRandomPositionInBoxCollider(GetComponent<BoxCollider>());

            Ball ball = CoreSceneCont.Instance.SpawnItem<Ball>(PrefabBall, pos, transform);
        }
        counter++;
    }
}
