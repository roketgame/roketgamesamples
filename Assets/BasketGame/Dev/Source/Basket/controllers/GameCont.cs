﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class GameCont : CoreGameCont
{
       
    public override void Init()
    {
        base.Init();

        CoreUiCont.Instance.OpenPage<PageIntro>();

    }


    public override void StartGame()
    {
        base.StartGame();

        CoreUiCont.Instance.OpenPage<PageGame>();
    }
}
