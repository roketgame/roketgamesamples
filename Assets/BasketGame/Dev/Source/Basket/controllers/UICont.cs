﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class UICont : CoreUiCont
{
    public override void Init()
    {
        base.Init();

        GetPageByClass<PageIntro>().EventStart.AddListener(onIntroPageStart);

    }

    private void onIntroPageStart()
    {
        Loaded();
    }
    public override void Load()
    {
        //base.Load();


    }




}
