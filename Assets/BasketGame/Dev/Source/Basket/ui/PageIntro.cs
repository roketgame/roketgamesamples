﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Events;

public class PageIntro : PageUI
{
    public class EventStartGame : UnityEvent { };
    public CoreUI BtnStart;
    public EventStartGame EventStart;
    public override void Init()
    {
        base.Init();
        EventStart = new EventStartGame();
        BtnStart.EventTap.AddListener(onStartClicked);
    }

    private void onStartClicked(TouchUI _info)
    {
        Debug.Log("clicked");
        //EventStart.Invoke();
        //CoreUiCont.Instance.Loaded();
    }

    public override void OnTouch(TouchUI info)
    {
        base.OnTouch(info);
        EventStart.Invoke();

    }


}
